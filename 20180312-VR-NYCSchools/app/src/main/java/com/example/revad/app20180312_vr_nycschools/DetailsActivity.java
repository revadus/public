package com.example.revad.app20180312_vr_nycschools;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
public class DetailsActivity extends AppCompatActivity {

    private TextView nameView;
    private TextView readingSATView;
    private TextView writingSATView;
    private TextView mathSATView;
    private TextView locationView;
    private TextView phoneView;
    private TextView websiteView;
    private String latValue;
    private String longValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //Extracting data passed from the main activity to display information about selected school
        Bundle extras = getIntent().getExtras();
        nameView = (TextView) findViewById(R.id.nameViewValue);
        nameView.setText(extras.getString("schoolNameDetails"));

        readingSATView = (TextView) findViewById(R.id.satReadingViewValue);
        readingSATView.setText(extras.getString("schoolReadingDetails"));

        writingSATView = (TextView) findViewById(R.id.satWritingViewValue);
        writingSATView.setText(extras.getString("schoolWritingDetails"));

        mathSATView = (TextView) findViewById(R.id.satMathViewValue);
        mathSATView.setText(extras.getString("schoolMathDetails"));

        locationView = (TextView) findViewById(R.id.locationViewValue);
        locationView.setText(extras.getString("schoolLocationDetails"));

        phoneView = (TextView) findViewById(R.id.phoneNumberViewValue);
        phoneView.setText(extras.getString("schoolPhoneDetails"));

        websiteView = (TextView) findViewById(R.id.websiteViewValue);
        websiteView.setText(extras.getString("schoolUrlDetails"));

        latValue = extras.getString("schoolLatitudeDetails");
        longValue = extras.getString("schoolLongitudeDetails");

    }

    //If a user clicks on school's address a map app opens
    public void onLocationViewClick(View view) {
        Uri gmmIntentUri = Uri.parse("geo:" + latValue + "," + longValue + "?q=" + nameView.getText());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    //If a user clicks on school's phone number a phone app opens
    public void onPhoneNumberViewClick(View view) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phoneView.getText().toString()));
        startActivity(callIntent);
    }

    //If a user clicks on school's website address a browser app opens
    public void onWebsiteViewClick(View view) {
        Uri schoolUri = Uri.parse(websiteView.getText().toString());
        Intent websiteIntent = new Intent(Intent.ACTION_VIEW, schoolUri);
        startActivity(websiteIntent);
    }
}
