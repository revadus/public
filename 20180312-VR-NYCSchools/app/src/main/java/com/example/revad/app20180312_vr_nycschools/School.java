package com.example.revad.app20180312_vr_nycschools;


/**
 * Created by revad on 3/9/2018.
 */

public class School {

    private String mLocation;
    private String mName;
    private String mUrl;
    private String mLatitude;
    private String mLongitude;
    private String mPhoneNumber;
    private String mReadingSAT;
    private String mWritingSAT;
    private String mMathSAT;

    //School class is used both for fetching schools list and school's SAT data, that's why its constructor includes all these parameters:
    public School (String name, String location, String url, String latitude, String longitude, String phoneNumber, String readingSAT, String writingSAT, String mathSAT) {
        mName = name;
        mLocation = location;
        mUrl = url;
        mLatitude = latitude;
        mLongitude = longitude;
        mPhoneNumber = phoneNumber;
        mReadingSAT = readingSAT;
        mWritingSAT = writingSAT;
        mMathSAT = mathSAT;
    }

    public String getName () {return mName;}

    public String getLocation () {return mLocation;}

    public String getUrl() {return mUrl;}

    public String getLatitude() {return mLatitude;}

    public String getLongitude() {return mLongitude;}

    public String getPhoneNumber() {return mPhoneNumber;}

    public String getReadingSAT() {return mReadingSAT;}

    public String getWritingSAT() {return mWritingSAT;}

    public String getmMathSAT() {return mMathSAT;}
}
