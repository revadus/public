package com.example.revad.app20180312_vr_nycschools;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<School>> {

    public static final String LOG_TAG = MainActivity.class.getName();
    private static final String SCHOOLS_REQUEST_URL = "https://data.cityofnewyork.us/resource/97mf-9njv.json";
    private static final String SAT_REQUEST_URL = "https://data.cityofnewyork.us/resource/734v-jeq5.json";
    private static final int LOADER_ID_GET_SCHOOLS_LIST = 1025473;
    private static final int LOADER_ID_GET_SCHOOL_SAT = 1025474;
    private SchoolsAdapter adapter;
    private TextView emptyStateView;
    private ProgressBar spinner;
    private List<School> searchList;
    private String schoolNameDetails;
    private String schoolLocationDetails;
    private String schoolUrlDetails;
    private String schoolLatitudeDetails;
    private String schoolLongitudeDetails;
    private String schoolPhoneDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView schoolsListView = (ListView) findViewById(R.id.list);
        emptyStateView = (TextView) findViewById(R.id.empty_state_view);
        spinner = (ProgressBar) findViewById(R.id.spinner_widget);
        schoolsListView.setEmptyView(emptyStateView);

        adapter = new SchoolsAdapter (this, new ArrayList<School>());
        schoolsListView.setAdapter(adapter);

        schoolsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                School currentSchool = adapter.getItem(position);
                schoolNameDetails=currentSchool.getName();
                schoolLocationDetails = currentSchool.getLocation();
                schoolUrlDetails = currentSchool.getUrl();
                schoolLatitudeDetails = currentSchool.getLatitude();
                schoolLongitudeDetails = currentSchool.getLongitude();
                schoolPhoneDetails = currentSchool.getPhoneNumber();
                getLoaderManager().initLoader(LOADER_ID_GET_SCHOOL_SAT, null, MainActivity.this);
            }
        });

        //Checking Internet connection
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (isConnected){
            getLoaderManager().initLoader(LOADER_ID_GET_SCHOOLS_LIST, null, this);
        }
        else
        {
            spinner.setVisibility(View.GONE);
            emptyStateView.setText(R.string.no_network);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //If Search menu item was clicked
        if (id == R.id.action_search) {

            final EditText txtUrl = new EditText(this);
            txtUrl.setHint("Enter school name");
            new AlertDialog.Builder(this)
                    .setTitle("Find school by name")
                    .setView(txtUrl)
                    .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            String findSchool = txtUrl.getText().toString();
                            int i=0;

                            //Clear the adapter to prepare it for filtered schools list
                            adapter.clear();
                            adapter.notifyDataSetChanged();

                            //Looking through the initial schools list saved to searchList to find schools matching the search criteria
                            while (i < searchList.size()) {
                                if (searchList.get(i).getName().toLowerCase().contains(findSchool.toLowerCase())) {

                                    //filling adapter with schools matching the search criteria
                                    adapter.add(searchList.get(i));
                                }
                                else
                                {
                                    //If a school does not match the serach criteria - remove it from the search list to narrow the search scope for future
                                    //(e.g. if a user will add another criteria to search among found schools)
                                    searchList.remove(i);
                                }
                                i++;
                            }
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    })
                    .show();

            return true;
        }

        if (id == R.id.action_reload) {
            //Reload is used to reset the search results to get back to the initial schools list
            getLoaderManager().restartLoader(LOADER_ID_GET_SCHOOLS_LIST, null, this);
        }
        return super.onOptionsItemSelected(item);
    }

    //We use Loader instead of just AsyncTask to avoid thread duplicating if the device state changes while the thread is being executed
    //(e.g. when the user rotates the device)
    @Override
    public Loader<List<School>> onCreateLoader(int i, Bundle bundle) {
        spinner.setVisibility(View.VISIBLE);

        //We need two loaders as we need to run two different fetch tasks: to get schools list and to get a school's SAT data
        if (i == LOADER_ID_GET_SCHOOL_SAT) {
            //If we need to fetch a schools SAT data we run loader with two parameters: URL and school name
            return new SchoolsListLoader(this, SAT_REQUEST_URL, schoolNameDetails);
        }
        //If we need to fetch schools list we run loader with only one parameter - URL
        return new SchoolsListLoader(this, SCHOOLS_REQUEST_URL);
    }

    @Override
    public void onLoadFinished(Loader<List<School>> loader, List<School> schools) {
        spinner.setVisibility(View.GONE);

        //After loader execution finished we need to process display the received data

        //If we received SAT data:
        if (loader.getId() == LOADER_ID_GET_SCHOOL_SAT) {
            String rSATExtra;
            String wSATExtra;
            String mSATExtra;

            //If a schools from the schools list is missing in the SAT list (there are some)
            //Then "No SAT data" will be displayed
            if (schools.size() < 1) {
                rSATExtra = getString(R.string.no_reading_sat);
                wSATExtra = getString(R.string.no_writing_sat);
                mSATExtra = getString(R.string.no_math_sat);
            }
            else
            {
                rSATExtra = schools.get(0).getReadingSAT();
                wSATExtra = schools.get(0).getWritingSAT();
                mSATExtra = schools.get(0).getmMathSAT();
            }
            Intent schoolDetailIntent;

            //Putting extra information into the intent for Details Activity
            schoolDetailIntent = new Intent(MainActivity.this, DetailsActivity.class);
            schoolDetailIntent.putExtra("schoolNameDetails", schoolNameDetails);
            schoolDetailIntent.putExtra("schoolLocationDetails", schoolLocationDetails);
            schoolDetailIntent.putExtra("schoolUrlDetails", schoolUrlDetails);
            schoolDetailIntent.putExtra("schoolLatitudeDetails", schoolLatitudeDetails);
            schoolDetailIntent.putExtra("schoolLongitudeDetails", schoolLongitudeDetails);
            schoolDetailIntent.putExtra("schoolPhoneDetails", schoolPhoneDetails);
            schoolDetailIntent.putExtra("schoolReadingDetails", rSATExtra);
            schoolDetailIntent.putExtra("schoolWritingDetails", wSATExtra);
            schoolDetailIntent.putExtra("schoolMathDetails", mSATExtra);
            startActivity(schoolDetailIntent);

            //Destroy the SAT fetch loader to avoid looping
            getLoaderManager().destroyLoader(LOADER_ID_GET_SCHOOL_SAT);
        }

        //If we received a list of schools:
        if (loader.getId() == LOADER_ID_GET_SCHOOLS_LIST) {
            adapter.clear();
            emptyStateView.setText(R.string.no_schools_found);
            if (schools == null) {
                return;
            }
            //searchList is used for search function, its purpose is to save an initial list of schools before the adapter is cleared to display a filtered list of schools
            searchList = schools;
            adapter.addAll(schools);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<School>> loader) {
        adapter.clear();
    }
}
