package com.example.revad.app20180312_vr_nycschools;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import static com.example.revad.app20180312_vr_nycschools.MainActivity.LOG_TAG;

/**
 * Created by revad on 3/9/2018.
 */

public class FetchData {

    private FetchData() {
    }

    //This method fetches a list of schools
    public static ArrayList<School> fetchSchoolsData(String requestUrl) {
        URL url = createUrl(requestUrl);

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }

        //Pass received JSON data to extract schools list and their details from JSON
        ArrayList<School> schools = extractSchools(jsonResponse);

        return schools;
    }

    //This methos fetches SAT data for a certain school
    public static ArrayList<School> fetchSATData(String requestUrl, String schoolName) {
        URL url = createUrl(requestUrl);

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }

        //Pass received JSON data and school name to the method that extract the data from JSON
        ArrayList<School> schools = extractSchoolSAT(jsonResponse, schoolName);

        return schools;
    }

    //Create a URL from the link passed from the Main activity
    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    //Open the connection and connect to the server
    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            Log.e(LOG_TAG, "Empty URL");
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //If connetion is successful
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    //Put data being received from the server into the String object
    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    //Extract schools' names and other data from JSON
    public static ArrayList<School> extractSchools(String schoolsJSON) {
        ArrayList<School> schools = new ArrayList<>();
        try {
            JSONArray features = new JSONArray(schoolsJSON);
            for (int i=0; i<features.length(); i++) {
                JSONObject properties = features.getJSONObject(i);
                String mName = properties.getString("school_name");

                String mLocation = properties.optString("location", "Not specified");
                mLocation = correctPostalAddress(mLocation);

                String mUrl = properties.optString("website", "Not specified");
                mUrl = correctWebsiteAddress(mUrl);

                String mLatitude = properties.optString("latitude", "0");
                String mLongitude = properties.optString("longitude", "0");
                String mPhoneNumber = properties.optString("phone_number", "Not specified");
                String readingSAT = null;
                String writingSAT = null;
                String mathSAT = null;
                schools.add(new School(mName, mLocation, mUrl, mLatitude, mLongitude, mPhoneNumber, readingSAT, writingSAT, mathSAT));
            }
        } catch (JSONException e) {
            Log.e("FetchData", "Problem parsing JSON results", e);
        }
        return schools;
    }

    //Initial postal address extracted from JSON needs to be corrected as it contains latitude and longitude data that we don't need to be there
    private static String correctPostalAddress (String postalAddress) {
        if (postalAddress.contains("(")) {
            int bracePos = postalAddress.lastIndexOf("(");
            postalAddress = postalAddress.substring(0, bracePos-1);
        }
        return postalAddress;
    }

    //If website address is issing a "http://" header - we add it there
    private static String correctWebsiteAddress (String websiteAddress) {
        if (!websiteAddress.startsWith("http:")) {
            websiteAddress = "http://" + websiteAddress;
        }
        return websiteAddress;
    }

    //Extract school's SAT data from JSON
    public static ArrayList<School> extractSchoolSAT(String schoolsJSON, String schoolName) {
        ArrayList<School> schools = new ArrayList<>();
        try {
            JSONArray features = new JSONArray(schoolsJSON);
            for (int i=0; i<features.length(); i++) {
                JSONObject properties = features.getJSONObject(i);
                String mName = properties.getString("school_name");

                //If "i" school's name matches the name of the school passed from the Main actitivty - we extract the school's SAT data from JSON
                if (mName.toLowerCase().contains(schoolName.toLowerCase()) | schoolName.toLowerCase().contains(mName.toLowerCase())) {

                    String readingSAT = properties.optString("sat_critical_reading_avg_score", "Not specified");
                    String writingSAT = properties.optString("sat_writing_avg_score", "Not specified");
                    String mathsSAT = properties.optString("sat_math_avg_score", "Not specified");
                    String mLocation = null;
                    String mUrl = null;
                    String mLatitude = null;
                    String mLongitude = null;
                    String mPhoneNumber = null;
                    schools.add(new School(mName, mLocation, mUrl, mLatitude, mLongitude, mPhoneNumber, readingSAT, writingSAT, mathsSAT));
                }
            }
        } catch (JSONException e) {
            Log.e("FetchData", "Problem parsing JSON results", e);
        }
        return schools;
    }
}
