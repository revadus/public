package com.example.revad.app20180312_vr_nycschools;

/**
 * Created by revad on 3/9/2018.
 */

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by revad on 3/9/2018.
 */

public class SchoolsAdapter extends ArrayAdapter<School> {

    public SchoolsAdapter(@NonNull Context context, List<School> schools) {
        super(context, 0, schools);
    }

    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        School currentSchool = getItem (position);

        //Adapter displays a list of schools that includes just schools' names
        TextView nameView = (TextView) listItemView.findViewById(R.id.nameView);
        nameView.setText(currentSchool.getName());

        return listItemView;
    }
}
