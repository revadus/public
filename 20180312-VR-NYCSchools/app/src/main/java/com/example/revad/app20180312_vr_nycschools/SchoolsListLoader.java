package com.example.revad.app20180312_vr_nycschools;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by revad on 3/9/2018.
 */

public class SchoolsListLoader extends AsyncTaskLoader<List<School>> {

    private String mUrl;
    private String mSchoolName;

    //SchoolsListLoader has an overloaded constructor as the same loader is used both for fetching schools list and school's SAT data

    //This one is for schools list:
    public SchoolsListLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    //This one is for school's SAT data
    public SchoolsListLoader(Context context, String url, String schoolName) {
        super(context);
        mUrl = url;
        mSchoolName = schoolName;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<School> loadInBackground() {
        if (mUrl == null) {
            return null;
        }
        ArrayList<School> schools;

        //If mSchoolName if empty then we want to fetch a schools list
        if ( TextUtils.isEmpty(mSchoolName)) {
            schools = FetchData.fetchSchoolsData(mUrl);
        } else {

            //Otherwise we want to fetch a SAT data for a certain school
            schools = FetchData.fetchSATData(mUrl, mSchoolName);
        }
        return schools;
    }
}
